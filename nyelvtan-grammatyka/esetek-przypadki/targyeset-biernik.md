# __Biernik__ (_tárgyeset_) - <sup>Kogo?</sup> <sub>Co?</sub>

## Végződések

![Eszközhatározó eset végződései](../media/img/targyeset_vegzodesek.png)

## Tárgyesettel kifejezendő esetek :thinking:

### A.) Prepozíciók

Néhány prepozíció tárgyesettel jár:

- __przez + bier.__: át, kereszül valamin; valaki/-mi miatt
  - Lecę do Polski __przez Słowację__. - _Lengyelországba repülök Szlovákián keresztül._
  - Musimy iść tam __przez rzekę__. - _Muszáj oda mennünk a folyón keresztül._
  - Boli mnie żołądek __przez stres__! - _Fáj a gyomrom a stressz miatt!_

- __w + bier.__: -ba, -be
  - Szándékosan nem írok ide példát, mert a lengyelben ez nem úgy van, mint a magyarban! Ha így látjuk, akkor tudjuk, hogy ezt jelenti, de később fogok a használatra példát írni, mikor nekem is világos lesz.  
  
- __na + bier.__: -ra, -re
  - Szándékosan nem írok ide példát, mert a lengyelben ez nem úgy van, mint a magyarban! Ha így látjuk, akkor tudjuk, hogy ezt jelenti, de később fogok a használatra példát írni, mikor nekem is világos lesz.

- __za + bier.__: múlva (ti. idő)
  - A: Dzień dobry, może mi pani pomóc? Kiedy odjedzie ten pociąg? - _Jó napot, tud nekem segíteni? Mikor indul ez a vonat?_
  - B: Dzień dobry, __za pięć__ minut! - _Jó napot, öt perc múlva!_

### B.) Igevonzatok

A legtöbb ige, ami a magyarban is tárgyesetet vonz, a lengyelben is ugyanígy viselkedik. Például:

- Lubię __mleko__. - _Szeretem a tejet._
- Czytam __ciekawą książkę__. - _Egy érdekes könyvet ovasok._
- Oglądają __smutny film__. - _Szomorú filmet néznek._

A teljesség igénye nélkül, tárgyesettel járó igék:

- __robić__ - _csinálni_
- __badać__ - _kutatni_
- __brać__ - _kézben vinni, magához venni (brać prysznic)_
- __budować__ - _építeni_
- __całować__ - _csókolni_
- __czytać__ - _olvasni_
- __jeść__ - _enni_
- __kupować__ - _venni_
- __kochać__ - _szeretni (szerelemmel)_
- __znać__ - _ismerni_
- __bić__ - _ütni_
- __malować__ - _festeni_
- __mieć__ - _birtokolni_
- __piec__ - _sütni_
- __lubić__ - _szeretni (enni, inni)_
- __witać__ - _üdvözölni_
- __zamykać__ - _zárni_
- __nieść__ - _kézben, gyalog vinni (szállítani)_
- __tłumaczyć__ - _fordítani (nyelvet)_
- __trzymać__ - _tartani (vmit vhol)_
- __spędzać__ - _tölteni (időt pl)_
- __ubierać__ - _öltöztetni_
- __chwalić__ - _dícsérni_

### C.) Napok

Tárgyesettel fejezzük ki, hogy melyik _napon_: Hétfőn, Kedden, stb., viszont ezen kívül máshol helyhatározó esetet kell használni, de ezzel most ne foglalkozzunk. Ezt __w + bier.__-el tudjuk kifejezni. Figyeljük meg, hogy a [__w wt..__]-t nehezen tudjuk kiejteni, ezért ott a [__w__]-hez egy kiejtéskönnyítő [__-e__] csatolódik.

- __w poniedzałek__ - _hétfőn_
- __we wtorek__ - _kedden_
- __w środę__ - _szerdán_
- __w czwartek__ - _csütörtökön_
- __w pjątek__ - _pénteken_
- __w sobotę__ - _szombaton_
- __w niedzielę__ - _vasárnap_

## Szabálytól való eltérések

Ragozásban, nőnemben egy kivétel van: __panią__ (_az asszonyt_). Itt [__-ą__]-t kap egy nőnemű főnév [__-ę__] helyett, ez az eszközhatározónál látott végződés.

A másik, amiről érdemes szólni, hogy vannak olyan igék, melyek a magyarban tárgyessettel járnak, de a lengyelben __nem__! Ilyen például a __słuchać__ (_hallgatni (vmit..)_), __życzyć__ (_kívánni (vmit..)_), __chcieć__ (_akarni (vmit..)_), ezek __britokos esettel__ járnak. Az utolsónál érdemes megfigyelni, hogy az élő nyelvben ez annyiban igaz, hogy az elvont fogalmak akarása esetén vagy indulatból való beszédkor használnak birtokos esetet, máskor pedig tárgyesetet.

Ezen kívül a [__-a__] véget kap néhány fix kifejés, tánctípusok, pénznemek, játékok, autó márkanevek, növények saját nevei (zöldség, gyümölcs, étel)..
