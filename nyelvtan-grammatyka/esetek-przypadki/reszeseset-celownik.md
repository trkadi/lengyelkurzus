# __WIP: Celownik__ (_részeseset_) - <sup>Komu?</sup> <sub>Czemu?</sub>

## Végződések

![Részeseset végződései](../media/img/reszeseset_vegzodesek.png)

## Helyhatározó esettel kifejezendő esetek :thinking:

### A.) Személyes névmások (_nekem, neked, .._)

A legmagátólértetődőbb használata a személyes névmásokban rejlik. Ezeknek létezik egy rövidebb és egy hosszabb változata. A rövid alakot igehasználat jellemzi, míg a hosszú alak hangsúlyt fektet az alanyra.

| Alanyeset       | Részeseset (_rövid_) | Részeseset (_hosszú_) |          |
| --------------: | :------------------: | :-------------------: | :------- |
| __ja__          | __mi__               | __mnie__              | _nekem_  |
| __ty__          | __ci__               | __tobie__             | _neked_  |
| __on/ona, ono__ | __mu, jej__          | __niemu, niej__       | _neki_   |
| __my__          | __nam__              | __nam__               | _nekünk_ |
| __wy__          | __wam__              | __wam__               | _nektek_ |
| __oni/one__     | __im__               | __nim__               | _nekik_  |

### B.) Igevonzatok

Bizonyos igék ezt az esetet vonzzák értelemszerűen. Vannak olyanok is azonban, melyek a józan magyar logikával nem egyeznek.

### C.) Kifejezések

Ez az eset leginkább kifejezésekben használatos, ebben nagyon gazdag a nyelv. A gond - bár nem nagy - azzal van, hogy ezeket nem tudjuk pontosan lefordítani magyarra. Hogy mit jelent ez arra nézzünk példát!
