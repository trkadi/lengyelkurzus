# __Narzędnik__ (_eszközhatározó eset_) - <sup>Kim?</sup> <sub>Czym?</sub>

Ez az eset nagyon hasznos a tanulás során, mert annak ellenére, hogy ennek vannak a legegyszerűbb végződései, hirtelen sokmindent tudunk vele kifejezni, pl. kik vagyunk, mi a foglalkozásunk, mik érdekelnek, mivel végezzük a dolgunk, stb..

## Végződések

![Eszközhatározó eset végződései](../media/img/eszkozeset_vegzodesek.png)

## Eszközhatározó esettel kifejezendő esetek :thinking:

### A.) Definiálás

A magyarban alanyesettel mondjuk, ha egy dolgot definiálunk: "Ez a férfi egy orvos." A lengyelben erre kétféle mód van, az egyik a __to__ (_ez / az_) szó használata, amely megegyezik a magyarral olyan értelemben, hogy minden alanyesetben áll.

- Tygrys __to__ kot. - _A tigris egy macska._
- Żelazo __to__ metal. - _A vas az fém._
- Metal __to__ materiał. - _A fém az anyag._

Ez nagyon egyszerű, viszont embereknél helytelenül, esetleg bunkón hangzik, ezért ilyen kontextusban nem célszerű használni. Van egy másik mód, ami jobban hasonlít az angolra, a létigével fejezendő ki és ugyanazt jelenti, mint az előbbi. Ilyenkor a definiált dolog alanyesetben marad, __de__ a __definiáló főnév eszközhatározó__ esetbe kerül! Ez a szabály nem csak a jelen időre vonatkozik, hanem ugyanúgy múlt és jövő időben is így használatos.

- Tygrys jest __kotem__. - _A tigris egy macska._
- Żelazo jest __metalem__. - _A vas az fém._
- Metal jest __materiałem__. - _A fém az anyag._
- Wera jest __kobietą__. - _Vera egy nő._
- Ona jest __moją siostrą__. - _Ő a (lány)testvérem._
- On jest __mężczyzną__. - _Ő egy férfi._
- Byłem __informatykiem__, ale teraz będę __księgowym__. - _Informatikus voltam, de most könyvelő leszek._

### B.) Igevonzatok

Bizonyos igék ezt az esetet vonzzák.

- __interesować się + narz.__: érdeklődni valami iránt
  - Czy __interesujesz się ogrodami?__ - _Érdekelnek téged a kertek?_
  - __Interesuję się dziewczynami.__ - _Engem a lányok érdekelnek._
- __zajmować się + narz.__: foglalkozni valamivel (foglalkozásszerűen)
  - __Czym się zajmujesz?.__ - _Mivel foglalkozol?_
  - __Zajmuję się programowaniem.__ - _Programozással foglalkozom._

### C.) Időhatározók

Bizonyos főnevek ebben az esetben időhatározó értelmet kapnak:

- czas (_idő_) → __czasem__ (_időnként_)
- lato (_nyár_) → __latem__ (_nyáron_)
- zima (_tél_) → __zimą__ (_télen_)

### D.) Személyes névmások

| Alanyeset|Eszközhat. eset|
|---------:|:--------------|
|        Ja|Mną            |
|        Ty|Tobą           |
|    On/Ono|Onym           |
|       Ona|Oną            |
|        My|Nim            |
|        Wy|Nią            |
|   Oni/one|Nimi           |

### E.) Prepozíciók

Egyes előljárószók ezzel az esettel járnak.

- __nad + narz.__: felett, (vízfelület felett is! → duna felett = dunaparton)
  - Ptaki lecą __nad górami__. - _A madarak a hegyek felett repülnek._
  - Spędzamy nasz urlop __nad Balatonem__. - _A szabadságunkat a Balatonnál (ti. a Balaton partján) töltjük._
- __pod + narz.__: alatt
  - Pies leży __pod stołem__. - _A kutya az asztal alatt fekszik._
- __przed + narz.__: előtt
  - ..i wtedy stałem tam __przed moją żoną__.. - _..és akkor ott álltam a feleségem előtt.._
- __za + narz.__: mögött, valamin túl
  - Dziecko jest __za drzewem__. - _A gyerek a fa mögött van._
  - Mieszkam __za Krakowem__. - _Krakkón túl lakom._
- __między + narz.__ _(+ a + narz. +..)_: között
  - Adam stoi __między Krzysztofem__ a __Kasią__. - _Ádám Kristóf és Kati közt áll._
- __poza + narz.__: (vmin) kívül
  - Wszyscy jedzą __poza Andrą__ - _Mindenki eszik Andrán kívül._
  - __Poza tym__, że jestem __dużym idiotą__, wszystko w porządku.. - _Azon kívül, hogy egy nagy idióta vagyok, minden rendben van._
- __z + narz.__: -val, -vel
  - Jadę do Kina __z Andrą__ - _Andrával megyek moziba._

Ugyanakkor létezik olyan is a magyarban, hogy pl. "_repülőVEL megyek_". __Nagyon fontos__, hogy abban az esetben, amikor a cselekvés eszköze az adott dolog, akkor __NEM SZABAD__ kitenni a [__z__]-t! Például:

- Lecę do Warszawy __samolotem__ a jadę do Krakowa __autobusem__. - _Varsóba repülővel repülők, Krakkóba meg busszal megyek._
- Piszę __długopisem__. - _Tollal írok._
