# Olvass el ;)

## Elérhetőségek

- Facebook: <https://www.facebook.com/trkadi/>
- Instagram: <https://www.instagram.com/trkadi/>

## Tartalomjegyzék

_Tipp_: A linkre kattintva mehetsz az egyes tartalmakhoz és a böngésző _vissza_ gombjával (**alt + balranyil**) tudsz visszajönni.

- **1.** [Tartalomjegyzék](README.md)
- **2.** Nyelvtan
  - **2.1.** Fő- és melléknevek (jelzők)
    - **2.1.1.** [Alanyeset](nyelvtan-grammatyka/esetek-przypadki/alanyeset-mianownik.md)
    - **2.1.2.** [Eszközhatározó eset](nyelvtan-grammatyka/esetek-przypadki/eszkozhatarozo_eset-narzednik.md)
    - **2.1.3.** [Tárgyeset](nyelvtan-grammatyka/esetek-przypadki/targyeset-biernik.md)
    - **2.1.4.** [Birtokos eset](nyelvtan-grammatyka/esetek-przypadki/birtokos_eset-dopelniacz.md)
    - **2.1.5.** [Helyhatározó eset](nyelvtan-grammatyka/esetek-przypadki/helyhatarozo_eset-miejscownik.md)
    - **2.1.6.** [Részeseset](nyelvtan-grammatyka/esetek-przypadki/reszeseset-celownik.md)
  - **2.4.** Számnevek
    - **2.4.1.** [Tőszámnevek (egy, kettő..)](nyelvtan-grammatyka/szamok-liczby/toszamnevek-liczba_kardynalna.md)
- [Linkek](links.md)
- [Markdown howto](markdown-help/README.md)
